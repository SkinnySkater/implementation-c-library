/*
** my_strstr.c for  in /home/toruser/Piscine/C4/lugard_o
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Thu Oct 19 18:38:14 2017 LUGARD OVILA JULIEN
** Last update Fri Oct 20 16:19:25 2017 LUGARD OVILA JULIEN
*/

char *my_strstr(char *str, char *to_find)
{
  int	i;
  int	j;

  i = 0;
  if (!*to_find)
    return (str);
  while (str[i])
    {
      j = 0;
      while (str[i + j] == to_find[j])
	{
	  if (to_find[j + 1] == '\0')
	      return (str + i);
	  j++;
	}
      i++;
    }
  return ('\0');
}
