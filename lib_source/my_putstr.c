/*
** my_putstr.c for  in /home/toruser/Piscine/C3/lugard_o
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Wed Oct 18 09:15:59 2017 LUGARD OVILA JULIEN
** Last update Wed Oct 18 09:16:51 2017 LUGARD OVILA JULIEN
*/

#include <unistd.h>

void	my_putstr(char *str)
{
  while (*str)
    {
      write(STDOUT_FILENO, &(*str), sizeof(*str));
      str++;
    }
}
