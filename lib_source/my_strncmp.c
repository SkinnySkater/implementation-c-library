/*
** my_strncmp.c for  in /home/toruser/Piscine/C4/lugard_o
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Thu Oct 19 12:19:51 2017 LUGARD OVILA JULIEN
** Last update Thu Oct 19 23:30:56 2017 LUGARD OVILA JULIEN
*/

int	my_strncmp(char *d, char *s, int n)
{
  int	i;

  i = 0;
  while (*d && *s && i < n)
    {
      if ((*d - *s) < 0)
	return (-1);
      if ((*d - *s) > 0)
	return (1);
      d++;
      s++;
      i++;
    }
  if (i == n || (*d == '\0' && *s == '\0'))
    return (0);
  if (*d == '\0')
    return (-1);
  else
    return (1);
}
