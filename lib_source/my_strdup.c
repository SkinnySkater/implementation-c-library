/*
** my_strdup.c for  in /home/toruser/Piscine/C6/lugard_o
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Mon Oct 23 11:20:42 2017 LUGARD OVILA JULIEN
** Last update Fri Oct 27 08:56:28 2017 LUGARD OVILA JULIEN
*/
#include <stdlib.h>

int	my_strlen(char *str);

char	*my_strcpy(char *dest, char *src);

char	*my_strdup(char *str)
{
  char	*dest;

  if (str == NULL)
    return (str);
  dest = malloc(sizeof(*dest) * (my_strlen(str) + 1));
  if (dest == NULL)
    return (dest);
  my_strcpy(dest, str);
  return (dest);
}
